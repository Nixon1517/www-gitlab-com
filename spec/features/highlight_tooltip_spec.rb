require 'spec_helper'

describe 'highlight-tooltip', :js do
  before do
    visit '/handbook/values'
  end

  context 'tooltip' do
    it 'shows correct button links in the tooltip' do
      # We need to wait for all the event listeners to be setup before beginning testing
      # Sometimes it can take longer than the default capybara 2-second wait timeout
      page.find('#highlight-tooltip[initialized]', visible: false, wait: 30)

      credit_header = page.find('#credit', text: 'CREDIT')

      select_text(credit_header)

      fire_mouse_up_event(credit_header)

      highlight_tooltip = page.find('#highlight-tooltip')

      # Twitter button
      expect(highlight_tooltip).to have_link(href: %r{https://twitter.com/intent/tweet})
      expect(highlight_tooltip).to have_link(href: %r{url=.+/handbook/values})
      expect(highlight_tooltip).to have_link(href: /text=%22CREDIT%22/)
      expect(highlight_tooltip).to have_link(href: /hashtags=gitlab,handbook/)

      # Web IDE button
      expect(highlight_tooltip).to have_link(href: %r{/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/source/handbook/values/index.html.md})

      # Edit button
      expect(highlight_tooltip).to have_link(href: %r{/gitlab-com/www-gitlab-com/blob/master/source/handbook/values/index.html.md})
    end
  end

  def select_text(element)
    js = <<-JAVASCRIPT
      const sel = window.getSelection()
      const range = window.document.createRange()
      const el = arguments[0].childNodes[0] // this should be the text node
      range.selectNode(el)
      sel.removeAllRanges()
      sel.addRange(range)
    JAVASCRIPT

    page.execute_script(js, element.native)
  end

  def fire_mouse_up_event(element)
    js = <<-JAVASCRIPT
      const el = arguments[0]
      const event = document.createEvent("HTMLEvents")
      event.initEvent("mouseup", true, true)
      event.eventName = "mouseup"
      el.dispatchEvent(event)
    JAVASCRIPT

    page.execute_script(js, element.native)
  end
end
