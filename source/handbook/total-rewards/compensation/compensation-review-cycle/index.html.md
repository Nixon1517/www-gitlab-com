---
layout: handbook-page-toc
title: Compensation Review Cycle
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

On this page, we explain how we review our Compensation Calculator and carry out the Compensation Review Cycle.

## Compensation Review Cycle

GitLab goes through 2 compensation reviews:

1. **Annual Compensation Review** - Happens in the fourth quarter of each year. This is when Compensation Calculator inputs are reviewed and when majority of compensation is reviewed for team members. Any changes will be processed with an effective date of February 1st.
1. **Catch-up Compensation Review** - For team members hired in November to January. The process starts in August with an effective date of September first.

## Annual Compensation Review

> The Annual Compensation Review process is currently undergoing iteration and the most up to date timeline can be reviewed in the [Total Rewards Schedule](/handbook/total-rewards/#total-rewards-schedule).

During the fourth quarter of each year, the Compensation & Benefits team will conduct a compensation review to ensure all team members are paid based on survey data in the [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator/). This is not a Cost of Living Adjustment, but instead a review of market changes. Location Factor and Compa Ratio will continue to be a part of the compensation calculator equation.

The annual compensation review is **not** directly linked to performance reviews nor the ability to be promoted. Other companies may tie compensation to performance reviews where each team member is rated on a performance scale with a corresponding compensation increase. GitLab’s takes a market based pay approach to annual compensation review by 1) assessing market changes in the compensation calculator inputs and 2) to review the Compa Group of team members to ensure alignment within the range.

The increase percentage may vary for each person. For instance, compa ratios are reviewed as part of a compensation adjustment or promotion, which GitLab encourages to happen independently from this review. If a team member was recently adjusted, the annual adjustment might yield no additional salary during the annual compensation review. This review acts as a sweep for each team member’s compensation to be evaluated at least once per year. If there is an increase in Location Factor and/or Benchmark, team members affected will have the new location factors applied to their compensation, but would not receive an automatic percent increase. For example, if the location factor in a region increases by 10% the team member will not receive an automatic 10% adjustment. The team member would receive an increase aligned to their benchmark, location factor, and compa group range taking any budget constraints into account.

#### Process overview

```mermaid
graph LR
    start((Annual review<br/>process<br/>kicks off))
    step_manager_review(Manager determines<br/>provisional compa group<br/>of team members)
    step_manager_discuss(Manager discusses<br/>provisional compa group<br/>with team members)
    step_manager_input(Manager inputs<br/>compa group<br/>into BambooHR)
    step_imanager_review(Indirect manager<br/>reviews<br/>compa groups)
    decision_imanager_approve{Approves<br/>compa groups?}
    step_imanager_agree(Indirect manager<br/>discusses the compa group<br/>with direct manager and<br/>determines agreed compa group)
    step_manager_inform(Manager informs<br/>direct report of approved<br/>compa group)
    step_exec_review_1(Executive review)
    decision_exec_change{Changes in compa group?}
    step_manager_discuss_change(Direct manager<br/>discusses feedback and<br/>changed compa group<br/>with direct report)
    step_cb_calculate(Comp & Benefits<br/>team calculates<br/>proposed increases)
    step_manager_compaas(Manager review<br/>in Compaas)
    step_exec_review_2(Executive review)
    step_manager_final(Manager informs<br/>direct reports of<br/>adjustment to<br/> compensation)
    stop((New<br/>compensation<br/>effective 1<br/>Feb 2020))

    start-->step_manager_review
    step_manager_review-->step_manager_discuss
    step_manager_discuss-->step_manager_input
    step_manager_input-->step_imanager_review
    step_imanager_review-->decision_imanager_approve
    decision_imanager_approve-->|No| step_imanager_agree
    decision_imanager_approve-->|Yes| step_manager_inform
    step_imanager_agree-->step_manager_inform
    step_manager_inform-->step_exec_review_1
    step_exec_review_1-->decision_exec_change
    decision_exec_change-->|No| step_cb_calculate
    decision_exec_change-->|Yes| step_manager_discuss_change
    step_manager_discuss_change-->step_cb_calculate
    step_cb_calculate-->step_manager_compaas
    step_manager_compaas-->step_exec_review_2
    step_exec_review_2-->step_manager_final
    step_manager_final-->stop
```

View the [enlarged version of the process](https://mermaidjs.github.io/mermaid-live-editor/#/view/eyJjb2RlIjoiZ3JhcGggTFJcbiAgICBzdGFydCgoQW5udWFsIHJldmlldzxici8-cHJvY2Vzczxici8-a2lja3Mgb2ZmKSkgXG4gICAgc3RlcF9tYW5hZ2VyX3JldmlldyhNYW5hZ2VyIGRldGVybWluZXM8YnIvPnByb3Zpc2lvbmFsIGNvbXBhIGdyb3VwPGJyLz5vZiB0ZWFtIG1lbWJlcnMpXG4gICAgc3RlcF9tYW5hZ2VyX2Rpc2N1c3MoTWFuYWdlciBkaXNjdXNzZXM8YnIvPnByb3Zpc2lvbmFsIGNvbXBhIGdyb3VwPGJyLz53aXRoIHRlYW0gbWVtYmVycylcbiAgICBzdGVwX21hbmFnZXJfaW5wdXQoTWFuYWdlciBpbnB1dHM8YnIvPmNvbXBhIGdyb3VwPGJyLz5pbnRvIEJhbWJvb0hSKVxuICAgIHN0ZXBfaW1hbmFnZXJfcmV2aWV3KEluZGlyZWN0IG1hbmFnZXI8YnIvPnJldmlld3M8YnIvPmNvbXBhIGdyb3VwcylcbiAgICBkZWNpc2lvbl9pbWFuYWdlcl9hcHByb3Zle0FwcHJvdmVzPGJyLz5jb21wYSBncm91cHM_fVxuICAgIHN0ZXBfaW1hbmFnZXJfYWdyZWUoSW5kaXJlY3QgbWFuYWdlcjxici8-ZGlzY3Vzc2VzIHRoZSBjb21wYSBncm91cDxici8-d2l0aCBkaXJlY3QgbWFuYWdlciBhbmQ8YnIvPmRldGVybWluZXMgYWdyZWVkIGNvbXBhIGdyb3VwKVxuICAgIHN0ZXBfbWFuYWdlcl9pbmZvcm0oTWFuYWdlciBpbmZvcm1zPGJyLz5kaXJlY3QgcmVwb3J0IG9mIGFwcHJvdmVkPGJyLz5jb21wYSBncm91cClcbiAgICBzdGVwX2V4ZWNfcmV2aWV3XzEoRXhlY3V0aXZlIHJldmlldylcbiAgICBkZWNpc2lvbl9leGVjX2NoYW5nZXtDaGFuZ2VzIGluIGNvbXBhIGdyb3VwP31cbiAgICBzdGVwX21hbmFnZXJfZGlzY3Vzc19jaGFuZ2UoRGlyZWN0IG1hbmFnZXI8YnIvPmRpc2N1c3NlcyBmZWVkYmFjayBhbmQ8YnIvPmNoYW5nZWQgY29tcGEgZ3JvdXA8YnIvPndpdGggZGlyZWN0IHJlcG9ydClcbiAgICBzdGVwX2NiX2NhbGN1bGF0ZShDb21wICYgQmVuZWZpdHM8YnIvPnRlYW0gY2FsY3VsYXRlczxici8-cHJvcG9zZWQgaW5jcmVhc2VzKVxuICAgIHN0ZXBfbWFuYWdlcl9jb21wYWFzKE1hbmFnZXIgcmV2aWV3PGJyLz5pbiBDb21wYWFzKVxuICAgIHN0ZXBfZXhlY19yZXZpZXdfMihFeGVjdXRpdmUgcmV2aWV3KVxuICAgIHN0ZXBfbWFuYWdlcl9maW5hbChNYW5hZ2VyIGluZm9ybXM8YnIvPmRpcmVjdCByZXBvcnRzIG9mPGJyLz5hZGp1c3RtZW50IHRvPGJyLz4gY29tcGVuc2F0aW9uKVxuICAgIHN0b3AoKE5ldzxici8-Y29tcGVuc2F0aW9uPGJyLz5lZmZlY3RpdmUgMTxici8-RmViIDIwMjApKVxuXG4gICAgc3RhcnQtLT5zdGVwX21hbmFnZXJfcmV2aWV3IFxuICAgIHN0ZXBfbWFuYWdlcl9yZXZpZXctLT5zdGVwX21hbmFnZXJfZGlzY3Vzc1xuICAgIHN0ZXBfbWFuYWdlcl9kaXNjdXNzLS0-c3RlcF9tYW5hZ2VyX2lucHV0XG4gICAgc3RlcF9tYW5hZ2VyX2lucHV0LS0-c3RlcF9pbWFuYWdlcl9yZXZpZXdcbiAgICBzdGVwX2ltYW5hZ2VyX3Jldmlldy0tPmRlY2lzaW9uX2ltYW5hZ2VyX2FwcHJvdmUgXG4gICAgZGVjaXNpb25faW1hbmFnZXJfYXBwcm92ZS0tPnxOb3wgc3RlcF9pbWFuYWdlcl9hZ3JlZVxuICAgIGRlY2lzaW9uX2ltYW5hZ2VyX2FwcHJvdmUtLT58WWVzfCBzdGVwX21hbmFnZXJfaW5mb3JtXG4gICAgc3RlcF9pbWFuYWdlcl9hZ3JlZS0tPnN0ZXBfbWFuYWdlcl9pbmZvcm1cbiAgICBzdGVwX21hbmFnZXJfaW5mb3JtLS0-c3RlcF9leGVjX3Jldmlld18xXG4gICAgc3RlcF9leGVjX3Jldmlld18xLS0-ZGVjaXNpb25fZXhlY19jaGFuZ2VcbiAgICBkZWNpc2lvbl9leGVjX2NoYW5nZS0tPnxOb3wgc3RlcF9jYl9jYWxjdWxhdGVcbiAgICBkZWNpc2lvbl9leGVjX2NoYW5nZS0tPnxZZXN8IHN0ZXBfbWFuYWdlcl9kaXNjdXNzX2NoYW5nZVxuICAgIHN0ZXBfbWFuYWdlcl9kaXNjdXNzX2NoYW5nZS0tPnN0ZXBfY2JfY2FsY3VsYXRlXG4gICAgc3RlcF9jYl9jYWxjdWxhdGUtLT5zdGVwX21hbmFnZXJfY29tcGFhc1xuICAgIHN0ZXBfbWFuYWdlcl9jb21wYWFzLS0-c3RlcF9leGVjX3Jldmlld18yXG4gICAgc3RlcF9leGVjX3Jldmlld18yLS0-c3RlcF9tYW5hZ2VyX2ZpbmFsXG4gICAgc3RlcF9tYW5hZ2VyX2ZpbmFsLS0-c3RvcCIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In19)

### Annual Compensation Review Timeline

1. Benchmarks and Location Factors
  * The Compensation & Benefits team will review all benchmarks and location factors associated with the Compensation Calculator and propose revised inputs to the Compensation Group for approval/implementation.
1. Compa Ratios
   * The Compensation & Benefits team will reach out the managers to obtain compa groups for each active team member with a hire date on or before October 31st.
   * Manager will then use the [Compa Group Guidelines](/handbook/total-rewards/compensation/compensation-calculator/#compa-group) to assess each team member's classification within their current position.
   * Managers will add this compa group as a request in BambooHR which is also approved by the manager's manager. Instructions on how to do so can be found in the [determining](/handbook/total-rewards/compensation/compensation-calculator/#determining) section.
   * Once all requests have been reviewed and approved the executive leaders of each group will review the breakdown with the Compensation & Benefits team along with the People Business Partner for their group.
     * In this breakdown, leaders will be able to analyze all individual compa groups, the distribution of compa groups overall and by department, as well as a gender distribution audit. Compensation will outline any concerns based on the aggregated data for the leader and people business partner to review.
   * Remember that this is also a good time to update your team’s position description if it does not reflect the role.
   * Compa Groups should be submitted to BambooHR no later than November 30th.
1. Using the revised inputs, the Compensation & Benefits team will calculate a proposed increase for each team member using the exchange rate as of 2020-01-01.
1. [Manager Review](/handbook/total-rewards/compensation/compensation-review-cycle/#manager-review) in Compaas
  * All indirect managers will also review the increases compared to their budget.
  * Each division leader is responsible for making sure their group stays within budget. The company has a 6% budget for all COLA and Market Adjustments.
  * Depending on budget constraints, the increases for the individual team members may be adjusted up or down by management and reviewed by Compensation & Benefits.
1. Once the People Group gives the ok: Managers will inform the team members of the increase and adjustments to compensation compared to their compa group.
1. Total Rewards Analysts will update [BambooHR](https://www.bamboohr.com/d/), and notify all payroll providers to be effective February 1st. Letters of adjustment are not necessary for annual compensation review changes.

### Manager Review

As part of the new [Annual Compensation Review](/handbook//total-rewards/compensation/compensation-review-cycle/#annual-compensation-review-timeline), managers will review, approve, and where necessary update the proposed salary increases to ensure that we are paying each team member to market. Please verify the compensation calculator inputs (compa group, level, title) are accurate.

It is very important that GitLab team-members understand their [compa group](/handbook/total-rewards/compensation/compensation-calculator/#compa-group) and how it impact their salary.

While some GitLab team-members may not receive an increase due to already being at the right [competitive rate](/handbook/total-rewards/compensation/#competitive-rate) for their Comp Group, Level, Role, and Location there are other circumstances where an increase should be avoided. If there are any reasons as to why the team member should not receive the proposed increase to be aligned with their experience and market in our calculator, please email total-rewards@ domain with the reasoning and cc your People Business Partner. This could be due to a current performance issue, pending termination, etc. If you would like to delay the increase, please outline a proposed plan of action and deadline. Team members who are currently struggling to perform at their current level should have that communicated clearly and the manager should consider delaying the increase until performance reaches a commendable level.

#### Manager Review Department Specific Considerations

[Compaas](https://www.compa.as/) is GitLab's compensation platform where managers can login, review, change, and submit their proposed increases during Annual Compensation Review. January 2020 is the first annual compensation review using Compaas. In the past all compensation reviews have been managed in Google Spreadsheets, which is not a scalable solution. As we go through the first Manager Review in Compaas, please post your feedback in the following [issue](https://gitlab.com/gitlab-com/people-group/Compensation/issues/80) so we can iterate on the process for the next compensation review.

**Individual Contributors and Managers:**

The following departments will be administered through [**Compaas**](manager-review-in-compaas). _Compaas will open to managers January 8th by the end of business Pacific Time. The cycle will close for all slate owners and approvers on January 22nd by the end of business Pacific Time._

* Engineering: Customer Support, Development, Infrastructure, Quality, Security, UX
  * Due to budgetary limits, Engineering managers will not have any additional discretionary budget to allocate to their team members. All team members will be increased to whichever amount is higher: 1. The low end of their compa group with location/benchmark changes or 2. Their compensation after COLA, if eligible. This means that Managers will still be able to login to Compaas and review the proposed increases from Compensation, but the [slates](#manager-review-in-compaas) will already be approved. Any discretionary budget will be allocated by the director or vp level.
* G&A: Accounting, Business Operations, Finance, Legal, People, Recruiting
* Marketing: Brand and Digital Design, Community Relations, Corporate Marketing, Digital Marketing, Field Marketing, Marketing Ops, Product Marketing
* Product: Product Management, Product Strategy
* Sales: Customer Solutions, Customer Success, Field Operations, Business Development

The following Departments will be administered through [**Google Spreadsheets**](#manager-review-in-spreadsheets) since the roles in Marketing and Sales listed are not within the compensation calculator. _Managers and Indirect Managers will receive a spreadsheet to review on January 10th by 12 pm Eastern Time. All spreadsheets must be finalized on January 24th by 12 pm Eastern Time._

* Sales: Channels, Commercial Sales, Enterprise Sales
* Marketing: Demand Generation
* Meltano: Meltano

**Directors/Distinguished:**

All directors, senior directors, and distinguished engineers will be reviewed separately in google sheets since we are changing the variable plan from 10% to 15% for directors in FY 2021. For more information on how directors will be converted over time, please review [director compensation](/handbook/total-rewards/compensation/#director-compensation) The budgetary pool will not be broken down by Division but instead as one director pool. _Managers and Indirect Managers will receive a spreadsheet to review on January 10th by 12 pm Eastern Time. All spreadsheets must be finalized on January 24th by 12 pm Eastern Time._ All manager input will be reviewed and approved by the compensation group before the FY 2021 compensation is finalized.

**Executive/VP/Fellow:**

All executive and fellow compensation will be reviewed in google spreadsheets by the compensation group and, if required per role, the board.

#### Manager Review in Compaas

**Process for slate owners:**

1. Navigate to [Compaas](https://app.compa.as/login) and select the option to sign in with Google.
1. Select to sign in with your originally assigned GitLab email address.
* If you select to sign in with an alias, you will receive an error and not be able to sign in. You can confirm your original GitLab email address on the Personal tab of BambooHR.
* ![incorrect email](/images/handbook/people-group/10_WrongEmail.png)
1. Once you have logged in, you will see your slate. This will have the status of your budget, along with a card for each of the team members you will be recommending a raise for.
1. The card will list the team member's name, role, benchmark role, level, [compa group](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#compa-group), compensation, adjusted compensation, discretionary raise, final compensation, and notes.
* ![slate owner](/images/handbook/people-group/1_owner_slate.png)
* The role is located under the team member's name. This is the team member's job title including level, for example, Senior or Associate.
* Underneath the role is the [benchmark role](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark). This is the job title that is used for determining the base compensation before other inputs such as level are evaluated. For example, the benchmark role for a Senior Backend Engineer is a Backend Engineer.
* Next to the team member's name is their level. This is a job code used internally that is unique to each role.
* Next to the level is the [compa group](/handbook/total-rewards/compensation/compensation-calculator/#compa-group). These were selected by managers for each of their reports in November.
* The Compensation Before is the number in gray and is the team member's current compensation. The  Compensation Adjusted is the recommended compensation based on changes to location factor, role benchmark, [Cost of Living Adjustment](https://about.gitlab.com/handbook/people-group/global-compensation/#cost-of-living-adjustment-cola) and bringing the team member to the bottom of the range if they received an increase in compa group.
* Discretionary Raise is money that can be allocated to a team member to bring them higher in their compa group range. This can be allocated as either a cash amount or percentage.
* The talk bubble at the right of the team member's card can be clicked on to leave a note on the employee's record. This can be used to record reasons for decisions, details about specific compensation change, or open questions for approvers or HR.
* ![note](/images/handbook/people-group/3_Note.png)
1. Clicking the background of the person's card expands it to reveal their compensation timeline. The timeline shows any historical compensation data Compaas has available for each person. Clicking the card again closes the timeline.
* ![timeline](/images/handbook/people-group/2_owner_slate_timeline.png)
1. The slate can be saved as a draft at any time. When you choose to "Save and Submit", you will see a screen summarizing your proposed raises. You may choose to cancel, leaving the slate in a draft state, or "Submit and Lock" which automatically submits your recommendations for approval.
* ![slate owner submit](/images/handbook/people-group/4_Owner_submit.png)
1. Once the slate has been submitted for approval, it will be locked. You will no longer be able to make changes, but while the annual compensation cycle is open you will be able to log in and review your team's compensation adjustments.
* If any changes are made by a slate approver, these will be visible in your review screen.
* ![slate owner locked](/images/handbook/people-group/5_Owner_locked.png)

**Process for approvers:**

1. Approvers will log in using the same instructions as slate owners.
1. Once logged in, you will be taken to a slate review screen. This page shows the status of your budget, along with a card for each of the slates you will be approving or editing. Each card will have the status of the slate, the name of the slate owner, number of reports, each individual slate's budget, how much of the budget has been allocated, how much of the budget is left, currency (all in USD), and associated notes accessible by clicking the talk bubble at the right of the card.
* ![approver review](/images/handbook/people-group/6_Approver_ready.png)
1. Clicking the slate name or the > arrow will take you to a slate. Slates that are not ready for approval will not be available to select. If you need to override a slate approval, please reach out to compensation as only admins can submit on behalf of another account. When you view a slate on your list, you will be able to edit the slate owner's recommendations and save them. Once you have saved, please refresh the page to see the changes applied to your budget, if applicable. For more information on the details located on each team member's card, please review item 4 in the process for slate owners above.
* ![approver review slate](/images/handbook/people-group/7_Approver_viewSlate.png)
1. Once you are finished making edits to a slate you are an approver for, you can choose to "Save & Approve". You will be taken to a screen summarizing the proposed raises. You may choose to cancel, leaving the slate unapproved, or "Submit and Approve" which automatically submits your recommendations for approval to anyone higher up in the approval chain. It is recommended that you keep all slates in draft form until you are ready to approve all slates in your group. Once you submit you will not longer be able to make any changes.
* ![approver save and approve](/images/handbook/people-group/8_Approver_saveandapprove.png)
1. After you have approved a slate, it will no longer be editable on your list of slates. Any budget allocated to the approved slate will be reserved and deducted from your budget total. After approving, you will still be able to visit the slate and view a current summary of any proposed compensation adjustments including any adjustments made by an approver higher up in the approval chain.
* ![approver locked](/images/handbook/people-group/9_Approver_approved.png)

#### Manager Review in Spreadsheets

If you have direct reports eligible for compensation review and are in a department which is being administered through google spreadsheets you will receive an email from the Total Rewards team with a link to this handbook section as well as your slate. If you have any indirect reports you will have two tabs that need attention: "Slate" and "Approver".

If anything on your spreadsheet is not working, has incorrect information, or a formula breaks please email total-rewards@ and we can review/adjust.

**Slate:**
1. All information on your spreadsheet is a formula with the exception of Column R `$ Input from Leader`. This is where you as the manager can enter in the dollar amount in discretionary funds (if applicable). Please enter a value (even if $0) so your manager can see your slate has been completed. You may also want to notify your manager that your slate is ready to go.
1. Once the manager enters in this amount, their manager will automatically be able to see the increases on their spreadsheets as the sheets dynamically interact.
1. In column V, `Discretionary Approved by Comp Group`, you can see the final OTE for your team member along with the dollar increase as well and percent increase in order to communicate to your direct reports. Please do not communicate any compensation numbers until you have received confirmation from the Total Rewards team that all budgets have been approved.
1. Please note that your recommendation may or may not be applied based on leader discretion (budgets are managed at the executive level).

**Approver:**
1. If you have indirect reports, you will have two tabs on your spreadsheet. The first is your slate, please see the directions above. The second is the Approver tab which will have information about all of your direct reports along with manager discretion values (if inputted).
1. All information on your spreadsheet is a formula with the exception of Column U `$ Input from Leader`. This is where  you as the manager can enter in the dollar amount in discretionary funds (if applicable).
1. Please review your Manager's input (in column R, `$ Input from Manager`) and if you agree, enter the same value into Column U. Please enter a value (even if $0), so your manager knows your slates have been reviewed. You may also want to notify your manager that your slate and approver tab is ready to go.
1. Please note that your recommendation may or may not be applied based on leader discretion (budgets are managed at the executive level).

**Final Review:**
1. The executive of each group will have access to the master spreadsheet and be able to review the final leader approved discretionary amount. They will then enter the final approved discretionary amount in column AR `Approved by Leader (or Comp Group)`. Please enter a number, even if it is 0. Once this number is populated, all slate and approver spreadsheets will be able to see the final result.

#### Communicating Compensation Increases

All increases for Annual Compensation Review will be finalized by Feb 1st. FY 2021 compensation will be uploaded into BambooHR no later than Feb 4th for payroll to make changes in all subsequent systems. The Total Rewards team will turn off the ability to see compensation in BambooHR using Employee or Contractor Self Service from Jan 27th until Feb 13th for Employees and until Feb 6th for Contractors. Managers will have the ability to communicate any increases to their team before end of day on the 6th for Contractors and end of day on the 13th for Employees.

Sample communication: "The manager review process for annual compensation review is complete. Based on your compa group evaluation of x, you are receiving an increase of x%."

Please note, the % increases in Compaas are rounded up to the nearest whole number. Please either communicate the amount increase or calculate the % increase to at least the hundredths place for communication: `((FY21 Salary-FY20 Salary)/FY20 Salary)`.

If your direct report has any questions on the calculation of their increase please feel free to have them reach out to the Total Rewards team.

#### Compensation Increases for FY 2022

In previous compensation review cycles, the maximum the team member was allocated was the output of their market compensation in line with the compensation calculator, whereas the minimum was the prorated cost of living adjustment. In FY 2022 compensation planning we will move away from the prorated cost of living adjustments. The Total Rewards Team will work through alternate options on how the minimum compensation increase is determined in the following [issue](https://gitlab.com/gitlab-com/people-group/Compensation/issues/93).

### FY 2022 Annual Compensation Review Budget

In FY21, the budget was 6% of all team members as of October 31, 2019. For the next compensation review which will take place from November 2020 to January 2021 to be effective for Fiscal Year 2022, we will separate the promotion and annual compensation review budget for the following conditions:

1. Promotion Budget
  * Assume 12% of the team is promoted with an average of a 10% increase to OTE.
1. Annual Compensation Review Budget
  * 6% of those who were not promoted. Therefore 6% of 88% of the population.
1. Relocation Budget
  * Relocations are not considered in the budget. Theoretically throughout the year, there would be a net zero from those moving to hire cost and lower cost regions.

**Calculating Budget Actuals**
1. Promotion Budget
   * At the beginning of each quarter, the Total Rewards team will calculate what the budget is for each Division to be managed by the executive of the group.
   * Take total OTE (TTC) as of the first day of the quarter x 12% x 10% / 4 to get to the actual for that quarter.
   * Any additional increases over budget must be approved by the CPO and CFO and may be taken from the following quarter's pool
   * Per Divison, any unused funds for promotions can be rolled over per quarter, if after Q4 there are unused funds they can be added to the annual compensation review budget.
   * Considerations when reviewing increases in BambooHR:
     * Any promotion with a percent increase of more than 10% to base salary must be approved by the CPO
     * Any promotion where the employee has had less than one year of tenure in the current level must be approved by the CPO
     * Any promotion to Director and above must be approved by the Compensation Group
   *  The Compensation & Benefits team will track and report on budgets throughout the quarter as part of the team member spend KPI
1. Annual Compensation Review Budget
  * Per Division take the total OTE (TTC) of the group as of October 31st x 88%. Remove any overspend or allocate additional budget per division from the promotion budget.
  * Verify totals with the Manager, FP&A.

## Catch-up Compensation Review

For team members hired between November 1st and January 31st, participating only in the annual compensation review may mean their compensation is not reassessed for up to 15 months. GitLab has incorporated a catch up review conducted in August for anyone hired in November, December, or January of the previous year. For example, any new hires in Nov 2019 - Jan 2020 would be reviewed in August 2020.

During the [annual compensation review](#annual-compensation-review), the budget for these team members is separated out to be used in August. If anyone would fall out of the compensation range, the team member would be adjusted immediately, but this would be deducted from the budget used in August.

No cost of living adjustments would be made at this time. This review will only take into account a compa group alignment (if any). Team members should not expect an increase, but instead understand that their compensation is being reviewed to ensure alignment to market.

All changes will follow the same process as the [annual compensation review timeline](/handbook//total-rewards/compensation/compensation-review-cycle//#annual-compensation-review-timeline) with an effective date of September 1st.
