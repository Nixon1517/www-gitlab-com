---
layout: markdown_page
title: "Use Case driven GTM"
---

<!--

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
-->

## Why - Use Case driven Go to market

When a customer / prospect embarks on a transformation, they typically will initiate a specific improvement project, which will address a specific problem or challenge they are facing.  

- **Value Drivers** describe the reasons **why**,

- **Use Cases** describe the specific **What** they are trying to change.
![Why Use cases](../images/why-usecases-overall.png)

### A **Customer Use Case:** is:
* A **customer problem** or **initiative**, that attracts **budget** , in **customer terms** .
* Often aligned to industry analyst **market coverage** (i.e. Gartner, Forrester, etc. write reports on the topic)
* Relatively stable over time.
* A **discrete problems** that we believe GitLab solves
* A **reason customers choose GitLab** (hence which we should seek out in prospects)


### Use case details
The [Customer Use-Cases](/handbook/use-cases/) page defines 8 customer use cases which are the typical problems that a customer selects Gitlab to solve.

The 8 current use cases are:
- Source Code Management (all project intellectual property - code, design, and more)
- Continuous Integration (how teams automate and streamline build and test to improve quality and velocity)
- DevSecOps (how teams shift security left and make it relevant throughout the delivery lifecycle)
- Simplify DevOps (making adoption of DevOps practices easier and streamlined, eliminating waste)
- GitOps (Adopting DevOps practices in managing infrastructure and operations - Infrastructure as code)
- Continuous Delivery (streamlining and automating delivering and deploying code to different environments)
- Agile Project and issue management (Make it easier for teams to collaborate and align on projects)
- Cloud Native (developing, building, and delivering cloud native/micro services based applications - Kubernetes)

Additional potential use cases
- Remote Development Team Management
- Portfolio management
- Repository management
- Application monitoring
- Incident management

In product marketing we focus on GitLab stages **and** we also develop and curate collateral that aligns with a buyer's journey supporting the use cases.

## Buyer's journey and typical collateral

As a prospect is determining that they have a specific problem to solve, they typically go through several stages of a [buyer's journey](/handbook/marketing/corporate-marketing/content/#content-stage--buyers-journey-definitions) which we hope ultimately leads them to selecting GitLab as their solution of choice.

| **Awareness** | **Consideration** | **Decision/Purchase** |
|----|----|----|
| Collateral and content designed to reach prospects in this stage of their journey should be focused on **educating them about the problems they are facing**, the business impact of their problems, and the reality that others are successfully solving the same problem | Collateral and content designed to reach prospects in this stage of their journey should be focused on **positioning GitLab as a viable and compelling solution to their specific problem.** | Collateral and content designed to reach prospects in this stage of their journey should be focused on key information that a buyer needs to **justify GitLab as their chosen solution**. |
| Typical **Awareness** Collateral | Typical **Consideration** Collateral | Typical **Decision/Purchase** Collateral |
|  **-** White papers describing the problem space <br> **-** Infographics illustrating the impact of the problem/challenge <br> **-** Analyst reports describing the problem/domain  <br> **-** Webinars focusing on the problem and how can be solved <br> **-** Troubleshooting guides to help overcome the problem  <br> **-** Analysis of public cases where the problem impacted an organization (i.e. outage, data loss, etc) | **-** White papers describing the innovative solutions to the problem <br> **-** Infographics illustrating the success and impact of solving the problem <br> **-** Analyst reports comparing different solutions in the market (MQ, Waves, etc) <br> **-** Webinars focusing on the success stories and how gitlab helped solve the problem <br> **-** Customer Case Studies, Videos, Logos, etc <br> **-** Solution Check Lists / Plans for how to solve the problem <br> **-** Comparisons between GitLab and other solutions | **-** ROI calculators <br> **-** Use case specific implementation guides <br> **-** Use Case migration guides (from xyz to GitLab) <br> **-** Getting Started info <br> **-** References and case studies |

## Buyer's Journey - Audience

In addition the **buyer's journey**, there are also different audiences in an organization with different needs at different times. The general model for the buyer's journey outlines kinds of collateral that is needed at different stages of the buying cycle. It is incredibly important to understand the needs of the audience when creating collateral. Executives, managers, and individual contributors will need different information to support their specific work.

![Buyer's Journey](./buyers-cycle-journey.png)

- **Executives** will need information about business value, risk, cost, and impact to support strategic objectives
- **Managers** will need more detailed information to drive planning, justification, and migration details.
- **Contributors** need technical details about how it works, how it's better, and how it's going to help them in the future.

### UseCase GTM Bill of Materials



![GTM-bom](../images/BOM-Activation.png)


### Market Requirements - downstream impacts

![Market Requirements](../images/market-requirements.png)

### Use Case GTM Bill of Material Priorities & Tracking


<div class="wide-sheet">
<iframe width="1300" height="1100" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTptvo1N7m8tjLSgJFOoy8l2fhnJmlyS83JhsT2DU6Ki3x1tcJ0Jd4Y4nvDnz0EbLEddAzpWHQYMWVs/pubhtml?gid=925009631&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
</div>


* [GTM Tracking Spreadsheet](https://docs.google.com/spreadsheets/d/1K78pI_sqS91o8fpMReCF5n7gf7tZjCFxU4PjX2uddxk/edit#gid=2033522968)

#### Use Case GTM Areas of Interest

* [Source Code Management (SCM)](/handbook/marketing/product-marketing/usecase-gtm/source-code-management/)
* [Continuous Integration (CI)](/handbook/marketing/product-marketing/usecase-gtm/ci/)
* Continuous Delivery (CD) **TBD**
* [Shift Left Security / DevSecOps](/handbook/marketing/product-marketing/usecase-gtm/devsecops/)
* Agile **TBD**
* [Simplify DevOps](/handbook/marketing/product-marketing/usecase-gtm/simplify-devops/)
* Cloud Native **TBD**
* [GitOps](/handbook/marketing/product-marketing/usecase-gtm/gitops/)


### Bill of Materials Material Details

* Product Marketing
* [Technical Marketing](bom/tmm.html)
* Competitive Intelligence
* Market Research and Customer Insights
* Partner and Channel Marketing

### Tracking Progress

We will use Epics and Milestones to track our overall progress of creating and activating these use cases.

1. This [Epic Roadmap](https://gitlab.com/groups/gitlab-com/marketing/-/roadmap?label_name%5B%5D=usecase-gtm&layout=MONTHS&scope=all&sort=start_date_asc&state=opened&utf8=%E2%9C%93_ ) view shows the current Epics that are actively being workd.
1. This [Milestone](https://gitlab.com/gitlab-com/marketing/product-marketing/-/milestones/11) is tracking all the issues in the Use Case GTM Sprint #3 (March 2020)

Overview of the setup in GitLab
