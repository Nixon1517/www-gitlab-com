---
layout: handbook-page-toc
title: "Technical Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose (Why)
Technical Marketing exists to create content meant to entice prospects into the top of the funnel by expressing the technical capabilities of the product as values to our target audiences.

## Process (How)
We do this by teaching our audiences about modern software delivery methods and how it can be valuable to them, introducing them to new concepts which can help them achieve their goals, and by showcasing the capabilities of the product for the use cases which our audiences care about.

## Output (What)
We produce demos, videos, workshops, tutorials, technical white papers, blogs, conference presentations, and webinars.

### Demos

One form of output is demos to help show the value GitLab can bring to customers. Go to the [Demo page](/handbook/marketing/product-marketing/demo/) to see what's available and get more info.

## Prioritizing work
Our work is mainly driven and prioritized by the following Marketing defined [Customer Use Cases](https://about.gitlab.com/handbook/use-cases/) and can be tracked on the [Use Case Driven GTM page](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/). Specifically, our deliverables contributing to this effort are specified on the [Technical Marketing BOM Elements page](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/bom/tmm.html).

When Analyst Reports come along then we prioritize providing and reviewing technical responses and most importantly we are the DRI for any demos that are required.

## What are we currently working on?
View the [Technical Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/926375?&label_name[]=tech-pmm) to see what the TMM team is working on.

## Making a Request
To make a request of the Technical Marketing team please [open an issue and fill in the Strategic Marketing Request template](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new?issuable_template=A-SM-Support-Request). We will respond to it using [our request management flow](https://about.gitlab.com/handbook/marketing/product-marketing/#requesting-strategic-marketing-team-helpsupport).

## Which Technical Marketing Manager?

Each TMM is listed with their areas of primary responsibility, but all TMM's should be able to help in other areas of the product as well:

  - [Tye Davis](/company/team/#TyeD19) - Agile, SCM (acting)
  - [Itzik Gan-Baruch](/company/team/#itzikgb), CI, CD (acting)
  - [Fernando Diaz](/company/team/#fjdiaz), DevSecOps, GitOps/IaC (acting)
  - [Hired! - starting in April](https://about.gitlab.com/jobs/apply/technical-marketing-manager-4380429002/) - SCM
  - [Hired! - starting in April](https://about.gitlab.com/jobs/apply/technical-marketing-manager-4380429002/) - GitOps/IaC
  - [Open Vacancy](https://about.gitlab.com/jobs/apply/technical-marketing-manager-4380429002/) - CD/Release
  - [Dan](/company/team/#dbgordon), Manager, TMM
  - [Ashish](/company/team/#kuthiala), Sr. Director, SM

### Technical Marketing Howto's
* Adding comparison pages ([instructions](/handbook/marketing/website/#creating-a-devops-tools-comparison-page), [video](https://youtu.be/LH4lKT-H2UU))
* [Using and creating simulation demos](https://about.gitlab.com/handbook/marketing/product-marketing/demo/sim-demos/)
* Creating a Google (GCP) GKE cluster for GitLab demo
* [Creating an AWS EKS cluster for a GitLab demo](./howto/eks-cluster-for-demo.html)
* [Conference booth demo setup](https://about.gitlab.com/handbook/marketing/product-marketing/demo/conference-booth-setup/)
