/label ~Frontend ~"trainee maintainer"
/assign `@manager`

<!-- Congratulations! Fill out the following MR when you feel you are ready to become -->
<!-- a frontend maintainer! This MR should contain updates to `data/team.yml` -->
<!-- declaring yourself as a maintainer of `gitlab-org/gitlab-ui` and -->
<!-- `gitlab-org/gitlab-com` -->

## Links to Non-Trival MRs I've Reviewed

<interesting MRs reviewed and discussions in trainee issue here>

## Links to Non-Trivial MRs I've Written

<interesting MRs authored here>

@gitlab-org/maintainers/frontend please chime in below with your thoughts, and
approve this MR if you agree.

## Once This MR is Merged

1. [ ] Create an [access request][access-request]
       for maintainer access to `gitlab-org`.
1. [ ] Let a maintainer add you to `gitlab-org/maintainers/frontend`
1. [ ] Announce it _everywhere_
1. [ ] Keep reviewing, start merging :sign_of_the_horns: :sunglasses: :sign_of_the_horns:

[access-request]: https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request
